#include <iostream>
#include "Config.h"

// In config.cpp

namespace config
{
    string FONT_FILE = "foo.ttf";
    string SOUND_FILE = "beep.wav";
    int timeout = 0;
    int def_item = 1;
    int font_size = 28;
    bool sound = true;
    bool window_mode = true;

	template <class T> bool load_value(Config &cfg,const string name,T& val) {
	  // Get the string value.
	  try
	  {
		T newValue = cfg.lookup(name);
		val = newValue;
	  }
	  catch(const SettingNotFoundException &nfex)
	  {
		cerr << "In config.cpp: No '"<<name<<"' setting in configuration file." << endl;
		return false;
	  }
	  catch(const SettingTypeException &stex)
	  {
		cerr << "In config.cpp: '"<<name<<"' setting's type missmatch." << endl;
		return false;  
	  }
	  return true;
	}

	bool load_config()
	{
		// Code to load and set the configuration variables
	  Config cfg;

	  // Read the file. If there is an error, report it and exit.
	  try
	  {
		cfg.readFile(CONFIG_FILE);
	  }
	  catch(const FileIOException &fioex)
	  {
		std::cerr << "In config.cpp: I/O error while reading file: "<< CONFIG_FILE << std::endl;
		return(EXIT_FAILURE);
	  }
	  catch(const ParseException &pex)
	  {
		std::cerr << "In config.cpp: Parse error at " << pex.getFile() << ":" << pex.getLine()
				  << " - " << pex.getError() << std::endl;
		return(EXIT_FAILURE);
	  }

	  // Get the font name.
	  load_value(cfg,"font_file",config::FONT_FILE);
	  load_value(cfg,"sound_file",config::SOUND_FILE);
	  load_value(cfg,"font_size",config::font_size);
	  load_value(cfg,"sound",config::sound);
	  load_value(cfg,"window",config::window_mode);
	  
	  return(EXIT_SUCCESS);
	}
}

// eof
