// In config.h

#ifndef CONFIG_H
#define CONFIG_H

#ifdef DEBUG
#ifndef CONFIG_FILE
#define CONFIG_FILE "config_dbg.ini"
#endif
#else
#ifndef CONFIG_FILE
#define CONFIG_FILE "SDLdialog/config.ini"
#endif
#endif

#include <string>
#include <libconfig.h++>

using namespace std;
using namespace libconfig;

namespace config
{
    extern std::string FONT_FILE;
    extern std::string SOUND_FILE;
    extern int timeout;
    extern int def_item;
    extern int font_size;
    extern bool sound;
    extern bool window_mode;

    bool load_config();
    template <class T> bool load_value(Config &cfg,const std::string name,T& val);
}

#endif
