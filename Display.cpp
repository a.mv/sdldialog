#include <vector>
#include <iostream>
#include <algorithm>

#include "Display.h"
#include "Menu.h"

Display::Display(bool windowMode)
{
    bool success = true;
    // Declare display mode structure to be filled in.
    SDL_DisplayMode current;
    SCREEN_WIDTH = 640;
    SCREEN_HEIGHT = 480;
	//Set texture filtering to linear
	if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
	{
		SDL_LogWarn( logCat,"Warning: Linear texture filtering not enabled!" );
	}
	//Create Window
	m_window = SDL_CreateWindow("Dialog", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (m_window == nullptr)
	{
		success = false;
		SDL_LogCritical(logCat,"Failed to create window.%s\n",SDL_GetError());
	}
	else
	{
		if(not windowMode) {
			if(SDL_SetWindowFullscreen(m_window,SDL_WINDOW_FULLSCREEN_DESKTOP) < 0) {
					SDL_LogWarn(logCat,"Failed set window full screen.%s\n",SDL_GetError());
			} else {
				if(SDL_GetCurrentDisplayMode(0, &current) != 0)
				// In case of error...
				 SDL_Log("Could not get display mode for video display #%d: %s", 0, SDL_GetError());
				 else {
				// On success, print the current display mode.
				SDL_Log("Display #%d: current display mode is %dx%dpx @ %dhz.", 0, current.w, current.h, current.refresh_rate);
				 SCREEN_WIDTH = current.w;
				 SCREEN_HEIGHT = current.h;	
				}
			}					
		}

	  //Create the renderer
		m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (m_renderer == nullptr)
		{
			success = false;
			SDL_LogCritical(logCat,"Failed to create the renderer.%s\n",SDL_GetError());
			int numdrivers = SDL_GetNumRenderDrivers (); 
			SDL_LogInfo(logCat,"Render driver count: %d",numdrivers); 
			for (int i=0; i<numdrivers; i++) { 
					SDL_RendererInfo drinfo; 
					SDL_GetRenderDriverInfo (1, &drinfo); 
					SDL_LogInfo(logCat,"Driver name (%d): %s",i,drinfo.name); 
					if (drinfo.flags & SDL_RENDERER_SOFTWARE) SDL_LogInfo(logCat," the renderer is a software fallback" ); 
					if (drinfo.flags & SDL_RENDERER_ACCELERATED) SDL_LogInfo(logCat," the renderer uses hardware acceleration"); 
					if (drinfo.flags & SDL_RENDERER_PRESENTVSYNC) SDL_LogInfo(logCat, " present is synchronized with the refresh rate" ); 
					if (drinfo.flags & SDL_RENDERER_TARGETTEXTURE) SDL_LogInfo(logCat, " the renderer supports rendering to texture" ); 
			} 
		}
	}
    if(! success ) throw std::runtime_error("Failed to create display");
}

void Display::render(Menu & menu)
{
    SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 0);
    SDL_RenderClear(m_renderer);

    //Render items
    for (size_t i=0;i<menu.size();i++)
    {
		SDL_Texture *item = menu.getItemTexture(i);
        SDL_RenderCopy(m_renderer, item, nullptr, menu.getItemPosition(i));
    }
    
    for(auto pb:m_progress_bars){
		pb->draw();
	}

    SDL_RenderPresent(m_renderer);
}

ProgressBar* Display::newProgressBar(int max, int current){
	const int height = 20;
	const int width = SCREEN_WIDTH;
	const SDL_Rect rect = {0,SCREEN_HEIGHT-height,width,height};
	ProgressBar* pb = new ProgressBar(m_renderer,rect,max);
	pb->setValue(current);
	m_progress_bars.push_back(pb);
	return pb;
}

void Display::deleteProgressBar(ProgressBar* pb){
	m_progress_bars.erase(std::remove(m_progress_bars.begin(), m_progress_bars.end(), pb), m_progress_bars.end());
	delete pb;
}


Display::~Display()
{
	//Destroy window	
	SDL_DestroyRenderer( m_renderer );
	SDL_DestroyWindow( m_window );
	for(auto pb:m_progress_bars){
		delete pb;
	}
}

void ProgressBar::draw(){
	if (! visible ) return;
	SDL_SetRenderDrawColor(m_renderer, 0, 255, 0, 30);
	if (SDL_RenderDrawRect(m_renderer,&m_rect) != 0){
		SDL_Log("Failed to draw rectangle.%s\n",SDL_GetError());
	} 
	SDL_Rect fill = m_rect;
	fill.w = fill.w*m_progress/m_total;
	SDL_RenderFillRect(m_renderer,&fill);
}
