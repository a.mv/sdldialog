#pragma once
#include <SDL.h>
#include <vector>
using namespace std;

class Menu;
class ProgressBar;
class Display
{
public:
    Display(bool);
    ~Display();

    void render(Menu& menu);

    SDL_Surface* getWindowSurface() { return m_windowSurface; }
    SDL_Renderer* getRenderer() { return m_renderer; }
    int getWidth(){return SCREEN_WIDTH;};
    int getHeight(){return SCREEN_HEIGHT;};
    ProgressBar* newProgressBar(int max){return newProgressBar(max,max);};
    ProgressBar* newProgressBar(int,int);
    void deleteProgressBar(ProgressBar*);

protected:
	//Screen resolution
    int SCREEN_WIDTH;
    int SCREEN_HEIGHT;
    
private:
    SDL_Window* m_window = nullptr;
    SDL_Surface* m_windowSurface = nullptr;
    SDL_Renderer* m_renderer = nullptr;
    const int logCat = SDL_LOG_CATEGORY_VIDEO;
	vector<ProgressBar*> m_progress_bars;

};

class ProgressBar{
	public: 
		ProgressBar(SDL_Renderer*rnd,SDL_Rect r,int t):m_renderer(rnd),m_rect(r),m_total(t),visible(false){};
		void setValue(int v){m_progress=v;};
		void draw();
		void show(){visible = true;};
		void hide(){visible = false;};
		
	private:
		SDL_Renderer* m_renderer;
		const SDL_Rect m_rect;
		const int m_total;
		int m_progress;
		bool visible;
		
	
};

