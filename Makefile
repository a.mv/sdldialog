#OBJS specifies which files to compile as part of the project
OBJS = Config.cpp Menu.cpp Display.cpp main.cpp Sound.cpp

#CC specifies which compiler we're using
CC = g++

#COMPILER_FLAGS specifies the additional compilation options we're using
# -w suppresses all warnings
COMPILER_FLAGS = -w -g

#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS = `pkg-config --cflags --libs "sdl2 SDL2_ttf SDL2_image libxdg-basedir libconfig++"`

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = SDLdialog

all:dialog
debug: COMPILER_FLAGS = -w -g -DDEBUG
debug: dialog

#This is the target that compiles our executable
dialog : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
	
playtest : 
	$(CC) playwawtest.cpp $(COMPILER_FLAGS) $(LINKER_FLAGS) -o playwawtest
	
configtest :
	$(CC) `pkg-config --cflags libconfig++` Config.cpp configtest.cpp -o configtest \
		`pkg-config --libs libconfig++`


.PHONY : clean
clean :
	-rm *.o *.gch $(OBJ_NAME)                       
