#include <iostream>
#include <basedir_fs.h>
#include "Menu.h"
#include "Display.h"
#include "Config.h"
using namespace std;

Menu::Menu(const vector<string>& labels, Display* display) {
	
	bool success = true;
	 //Initialize SDL_ttf
	if( TTF_Init() == -1 )
	{
		SDL_LogCritical(logCat,"SDL_ttf could not initialize! SDL_ttf Error: %s ",TTF_GetError());
		success = false;
	} else {
		SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );
		//Open the font
		char* font_path = xdgDataFind(m_menuFont.c_str(), NULL);
		if(strcmp(font_path, "") == 0) {
			SDL_LogCritical(logCat,"Cannot find menu font %s",m_menuFont.c_str());
			success = false;
		} else {
			m_Font = TTF_OpenFont(font_path , config::font_size );
			if( m_Font == NULL )
			{
				SDL_LogCritical(logCat,"Failed to load font! SDL_ttf Error: %s",TTF_GetError());
				success = false;
			}
		}
	}
	if(! success ) throw std::runtime_error("Cannot create menu");
	
	m_positions.reserve(labels.size());
	m_textures.reserve(labels.size());
	m_labels = labels;
	
	
	int w, h;
	int x = display->getWidth() / 2;
	
	int y = 0;
	
	for(auto l:labels){
		SDL_Texture* t=nullptr;
		t = loadFromRenderedText(l,display->getRenderer(),m_textColor);
		SDL_QueryTexture(t, NULL, NULL, &w, &h);
		if (y == 0){ //position items in the middle using the fist item's height
			int menu_size = labels.size()*h;
			y = display->getHeight()/2-menu_size/2;
		}
		SDL_Rect position = {x-w/2,y ,w,h};
		y += h;
		m_positions.push_back(position);
		m_textures.push_back(t);
	}	
	
}


SDL_Texture* Menu::loadFromRenderedText( const std::string textureText, SDL_Renderer* renderer, const SDL_Color textColor )
{

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( m_Font, textureText.c_str(), textColor );
	SDL_Texture* texture;
	if( textSurface == NULL )
	{
		SDL_LogCritical(logCat,"Unable to render text surface! SDL_ttf Error: %s",TTF_GetError());
	}
	else
	{
		//Create texture from surface pixels
        texture = SDL_CreateTextureFromSurface( renderer , textSurface );
		if( texture == NULL )
		{
			SDL_LogCritical(logCat,"Unable to create texture from rendered text! SDL Error: %s",SDL_GetError());
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}
	
	//Return success
	return texture;
}


int Menu::selectItem(int x, int y, Uint8 r, Uint8 g, Uint8 b){
	int item = 0;
	for(size_t i =0; i<m_textures.size(); i++){
		if(x>m_positions[i].x and x<(m_positions[i].x+m_positions[i].w) and 
			y>m_positions[i].y and y<(m_positions[i].y+m_positions[i].h) ) {
			SDL_SetTextureColorMod( m_textures[i], r,g,b);
			item = i+1;	
		} else {
			SDL_SetTextureColorMod( m_textures[i], m_textColor.r,m_textColor.g,m_textColor.b);
		}
	}
	return item;
}

void Menu::selectItem(int item, Uint8 r, Uint8 g, Uint8 b){
	item --;
	for(size_t i =0; i<m_textures.size(); i++){
		if(i == item) {
			SDL_SetTextureColorMod( m_textures[i], r,g,b);
		} else {
			SDL_SetTextureColorMod( m_textures[i], m_textColor.r,m_textColor.g,m_textColor.b);
		}
	}
}

void Menu::select(size_t item){
	if(item > m_textures.size() or item < 1) {
		m_selected = 0;
		return;
	}
	selectItem( item , m_hoverColor.r,m_hoverColor.g,m_hoverColor.b);
	m_selected = item;
}

int Menu::hover(int x, int y){
	m_selected = selectItem(x,y,m_hoverColor.r,m_hoverColor.g,m_hoverColor.b);
	return m_selected;
}

int Menu::click(int x,int y){
	m_selected =  selectItem(x,y,m_clickColor.r,m_clickColor.g,m_clickColor.b);
	return m_selected;
}

Menu::~Menu() {
	//Free loaded images
	for(auto t: m_textures)	SDL_DestroyTexture(t);

	//Free global font
	TTF_CloseFont( m_Font );
	TTF_Quit();
}
