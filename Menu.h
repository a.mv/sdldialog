#pragma once
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <vector>
#include "Config.h"
using namespace std;

class Display;
class Menu
{
public:
    Menu(const vector<string>& labels, Display* display);
    ~Menu();

    SDL_Texture* getItemTexture(int i) { return m_textures[i]; }
    SDL_Rect* getItemPosition(int i) { return &m_positions[i]; }
    size_t size() {return m_labels.size();};
    string getItemText(int i) {return m_labels[i];};
    
    int click(int x,int y);
	int hover(int x,int y);
	void select(size_t);
	int getSelected(){return m_selected;};
protected:


private:
	
	int selectItem(int x, int y, Uint8 r, Uint8 g, Uint8 b);
	void selectItem(int item, Uint8 r, Uint8 g, Uint8 b);
	int m_selected;
	
	SDL_Color m_textColor = { 255, 255, 255 };
	SDL_Color m_hoverColor = { 0, 255, 0 };
	SDL_Color m_clickColor = { 255, 0, 0 };
	
	string m_menuFont = config::FONT_FILE;

	SDL_Texture* loadFromRenderedText( const std::string textureText, SDL_Renderer* renderer, const SDL_Color textColor );
    
    TTF_Font *m_Font = nullptr;
	const int logCat = SDL_LOG_CATEGORY_CUSTOM;
    vector<SDL_Texture*> m_textures;
    vector<SDL_Rect> m_positions;
	vector<string> m_labels;
};
