#include "Sound.h"

/*
** PLAYING A SOUND IS MUCH MORE COMPLICATED THAN IT SHOULD BE
*/
Sound::Sound(const string& path){
	
	SDL_zero(m_wavSpec);
	add(path);
	if (SDL_WasInit(SDL_INIT_AUDIO) == 0) {
		//~ 
		//~ m_wavSpec.freq = 44100;
		//~ m_wavSpec.format = AUDIO_F32;
		//~ m_wavSpec.channels = 2;
		m_playAudio = false;
		if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0) {
			SDL_LogCritical(logCat,"Failed to initialize audio.%s",SDL_GetError());
			return;
		} 
		SDL_GetNumAudioDevices(0);
		m_dev = SDL_OpenAudioDevice(NULL, 0, &m_wavSpec, NULL, 0);
		if (m_dev == 0) {
			SDL_LogCritical(logCat,"Failed to open audio: %s", SDL_GetError());
			return;
		}	
		SDL_LogDebug(logCat,"Audio device : %d (%s)",m_dev,SDL_GetAudioDeviceName(m_dev, 0));
		m_playAudio = true;
	}
}
int Sound::add(const string& muspath) {
	if(m_playAudio || m_wavs.size() == 0){
		/* Load the WAV */
		// the specs, length and buffer of our wav are filled
		Wav w;
		if( SDL_LoadWAV(muspath.c_str(), &m_wavSpec, &w.wavBuffer, &w.wavLength) == NULL ){
			SDL_LogCritical(logCat,"Failed to load file %s : %s",muspath.c_str(),SDL_GetError());
			return 0;
		} else {
			m_wavs.push_back(w);
			return m_wavs.size();
		}
	}
	return 0;
}
void Sound::play(int i){
	if (not m_playAudio || (i == 0)) return;
	//if (SDL_GetAudioDeviceStatus(m_dev) == SDL_AUDIO_PLAYING) return;
	if(SDL_GetQueuedAudioSize(m_dev) > 0) {
		SDL_LogDebug(logCat,"Still playing on device : %d ",m_dev);
		return;
	} else {
		SDL_LogDebug(logCat,"Play on device %d item %d",m_dev,i);
	}
	if(SDL_QueueAudio(m_dev, m_wavs[i-1].wavBuffer, m_wavs[i-1].wavLength) < 0) SDL_LogWarn(logCat,"Playback error : %s", SDL_GetError());
	SDL_PauseAudioDevice(m_dev, 0);
}

Sound::~Sound (){
	// shut everything down
	if(m_playAudio) {
		for(auto w:m_wavs)
			SDL_FreeWAV(w.wavBuffer);
	}
	if (m_dev > 0) {
		SDL_PauseAudioDevice(m_dev, 0);
		//SDL_Delay(5000);  // let audio callback run for 5 seconds.
		SDL_CloseAudioDevice(m_dev);
		SDL_QuitSubSystem(SDL_INIT_AUDIO);
	}
}

