#include <SDL2/SDL.h>
#include <string>
#include <vector>

using namespace std; 

struct Wav {
	Uint32 wavLength;// length of our sample
	Uint8 *wavBuffer;// buffer containing our audio file
};

class AbstractSound
{
public:
    void play() {play(1);};
    virtual void play(int){};
    virtual int add(const string&){return 0;};
    bool canPlay() { return m_playAudio; };
    virtual ~AbstractSound() {};
protected:
	bool m_playAudio;

};

class DummySound : public AbstractSound
{
public:
    DummySound(){m_playAudio = false;};
    void play(int){};
    int add(const string&){ return 0;};
};

class Sound : public AbstractSound
{
public:
    Sound(const string&);
    ~Sound();
    void play(int);
    int add(const string&);
    

private:
	//~ void audio_callback(void *userdata, Uint8 *stream, Uint32 len);
	SDL_AudioSpec m_wavSpec;// the specs of our piece of music
	SDL_AudioDeviceID m_dev;
	//~ Uint8 *m_audioPos; // global pointer to the audio buffer to be played
	//~ Uint32 m_audioLen; // remaining length of the sample we have to play
	vector<Wav> m_wavs; // the specs of our piece of music
	//~ Uint32 m_wavLength; // length of our sample
	//~ Uint8 *m_wavBuffer; // buffer containing our audio file
	const int logCat = SDL_LOG_CATEGORY_AUDIO;

};

