#include "Display.h"
#include "Menu.h"
#include "Sound.h"

#include <SDL.h>

#include <getopt.h>
#include <basedir_fs.h>
#include <iostream>
using namespace std;
#include <regex>

#include "Config.h"
#include "user_events.h"

#ifdef DEBUG
#define LOG_LEVEL SDL_LOG_PRIORITY_DEBUG
#else
#define LOG_LEVEL SDL_LOG_PRIORITY_CRITICAL
#endif

std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

void checkSDLError(int line = -1)
{
    const char *error = SDL_GetError();

    if (*error != '\0') {
        cout << "SDL Error: " << error << " line: " << line << endl;
        SDL_ClearError();
    }
}

void PrintHelp()
{
    std::cout <<
            "--default  -d <n>   :  Set default item\n"
            "--timeout  -t <sec> :  Set timeout in seconds\n"
            "--nosound  -q       :  Don't beep the user\n"
            "--help     -h       :  Show help\n";
    exit(0);
}
void printAndFreeString(const char *string)
{
	std::cout<<string<<std::endl;
	free((char*)string);
}
struct Options {
	int DEFAULT;
	int timeout;
	bool sound;
	bool windowMode;
	int params;
};
Options ProcessArgs(int argc, char** argv);

int main( int argc, char* args[] )
{
	const int logCat = SDL_LOG_CATEGORY_APPLICATION;
	//Initialize SDL Video
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
    {
        SDL_LogCritical(SDL_LOG_CATEGORY_VIDEO,"SDL failed to initialize.%s\n",SDL_GetError());
        exit(255);
    }
	
	config::load_config();
	Options opts = ProcessArgs(argc,args);
	
	vector<string> labels;
	bool paramMenu = false;
    for (auto i = opts.params;i<argc;i++) labels.push_back(args[i]);
    for (auto i = argc-1;i>=0;i--) 
		if(strcmp("--",args[i]) == 0) paramMenu = true;
	if (labels.size() == 0 and paramMenu)
		for (std::string line; std::getline(std::cin, line);) {
			std::string value = std::regex_replace(line, std::regex("^ +| +$|( ) +"), "$1");
			if (value.size() > 0) labels.push_back(value);
		}
	;
	if (labels.size() == 0) labels = {"Continue","Exit"};
	

	
	SDL_LogSetAllPriority(LOG_LEVEL);
	Display *display = new Display(opts.windowMode);
    Menu *menu = new Menu(labels,display);
    
    menu->select(opts.DEFAULT);
    
    AbstractSound *snd;
    char * sound_file = xdgDataFind(config::SOUND_FILE.c_str(), NULL);
    if(strcmp(sound_file, "") == 0){
		SDL_Log("Beep file not found, sound disabled");
		opts.sound = false;
	} 
    if(opts.sound) {
		snd = new Sound(sound_file);
		SDL_LogDebug(logCat,"Sound enabled");
	} else {
		snd = new DummySound();
		SDL_LogDebug(logCat,"Sound disabled");
	} 
	free(sound_file);
    
    //Event handler
	SDL_Event e;
	//Main loop flag
	bool quit = false;
	bool keyPress = false;
	int x,y,item=0;
    //While application is running

    SDL_TimerID my_timer_id; 
    ProgressBar* p_bar;
    if(opts.timeout > 0) {
		my_timer_id = SDL_AddTimer(1000, timeoutQuitUserEvent, &opts.timeout);
		p_bar = display->newProgressBar(opts.timeout);
		p_bar->show();
	}
    
	while( !quit )
	{
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{
			switch(e.type)
			{
			case SDL_USEREVENT: {
				switch(e.user.code){
					case USER_EVENT_QUIT:
						quit=true;
					break;
					case USER_EVENT_TICK:
						p_bar->setValue(opts.timeout);
					break;
				}
            break;
			}
			case SDL_QUIT:
				//User requests quit
				quit = true;
			break;
			case SDL_KEYUP:
					keyPress = false; 
			break;
			case SDL_KEYDOWN:
				//std::cout<<SDL_GetKeyName(e.key.keysym.sym)<<std::endl;
				if(opts.timeout > 0){
					SDL_RemoveTimer(my_timer_id);
					opts.timeout = 0;
					p_bar->hide();
				}
				switch ( e.key.keysym.sym ) 
				{
					case SDLK_ESCAPE: 
					case SDLK_q: 
						menu->select(0);
						quit = true;
					break;
					case SDLK_KP_ENTER:
					case SDLK_RETURN:
					case SDLK_SPACE:
					if ( !keyPress ) 
					{
						quit = true;
						//something
					}
					break;
					case SDLK_DOWN: 
					if (!keyPress) 
					{
						keyPress = true;
						int i = menu->getSelected() + 1;
						if( i <= menu->size() ){
							menu->select(i);
						}
					}
					break;
					case SDLK_UP: 
					if (!keyPress) 
					{
						keyPress = true;
						int i = menu->getSelected() - 1;
						if( i > 0 ){
							menu->select(i);
						}
					}
					break;
					default: 
					break;
				}
			break;
			case SDL_MOUSEMOTION:
				if(opts.timeout > 0){
					SDL_RemoveTimer(my_timer_id);
					opts.timeout = 0;
					display->deleteProgressBar(p_bar);
				}
				menu->hover(e.motion.x,e.motion.y);
			break;

			case SDL_MOUSEBUTTONDOWN:
				x=e.button.x;
				y=e.button.y;
				item = menu->click(x,y);
				if(item > 0) {
                    quit=true;
				}
			break;
			}
		}

		display->render(*menu);
		int selected = menu->getSelected();
		if (selected != item and selected != 0) {
			snd->play();
			item = selected;
		}
	}
	

	//Quit SDL subsystems
	item = menu->getSelected();
	SDL_LogDebug(SDL_LOG_CATEGORY_VIDEO,"Exit value : %d\n",item);
	delete menu;
	delete snd;
	delete display;
	SDL_Quit();
	return item;
}

Options ProcessArgs(int argc, char** argv)
{
	Options o = {0,0,config::sound,config::window_mode};
    const char* const short_opts = "d:qt:hw";
    const option long_opts[] = {
            {"default", required_argument, nullptr, 'd'},
            {"nosound", no_argument, nullptr, 's'},
            {"window", no_argument, nullptr, 'w'},
            {"timeout", required_argument, nullptr, 't'},
            {"help", no_argument, nullptr, 'h'},
            {nullptr, no_argument, nullptr, 0}
    };
    
    bool default_defined = false;

    while (true)
    {
        const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
        int num;

        if (-1 == opt)
            break;

        switch (opt)
        {
        case 'd':
            num = std::stoi(optarg);
            o.DEFAULT = num;
            default_defined = true;
            break;

        case 't':
            num = std::stoi(optarg);
            o.timeout = num;
            break;

        case 'q':
            o.sound = false;
            break;
            
        case 'w' :
			o.windowMode = true;
			break;

        case 'h': // -h or --help
        case '?': // Unrecognized option
        default:
            PrintHelp();
            break;
        }
    }
    o.params = optind;
    if(! default_defined) o.timeout = 0;
    return o;
}
