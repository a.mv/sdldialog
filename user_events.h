#define USER_EVENT_QUIT 0
#define USER_EVENT_TICK 1

Uint32 timeoutQuitUserEvent(Uint32 interval, void *param)
{
    SDL_Event event;
    SDL_UserEvent userevent;
    int* timeout = (int*) param;

    /* In this example, our callback pushes an SDL_USEREVENT event
    into the queue, and causes our callback to be called again at the
    same interval: */

    userevent.type = SDL_USEREVENT;
    if(*timeout > 0){
		*timeout -= 1;
		userevent.code = USER_EVENT_TICK;
	} else {
		userevent.code = USER_EVENT_QUIT;
	}
    userevent.data1 = NULL;
    userevent.data2 = NULL;

    event.type = SDL_USEREVENT;
    event.user = userevent;
    SDL_PushEvent(&event);
    if (userevent.code == USER_EVENT_QUIT) {
		return(0);
	} else {
		return(1000);
	}
}
